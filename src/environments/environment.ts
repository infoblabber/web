// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://myblabber.com/staging-v2/api/',
  getUser: 'get_user',
  signIn: 'sign-in',
  signUp: 'sign-up',
  editUser: 'edit-profile',
  changeProfilePhoto: 'change-profile-photo',
  getCategories: 'get-categories',
  searchBusinesses: 'search-businesses',
  getReviews: 'get-reviews',
  addReaction: 'add-reaction',
  addComment: 'comment',
  addReview: 'review',
  saveBusiness: 'save_business',
  getAreas: 'get-areas'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
