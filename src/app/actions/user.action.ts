export class UserLoginAction {
  static readonly type = '[User] Login';
  constructor(public payload: any) { }
}

export class UserRegisterationAction {
  static readonly type = '[User] Register';
  constructor(public payload: any) { }
}

export class EditUserAction {
  static readonly type = '[User] Edit';
  constructor(public payload: any) { }
}

export class AreasAction {
  static readonly type = '[Areas] getAreas';
  constructor(public payload: any) { }
}


