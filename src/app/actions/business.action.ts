export class CategoriesAction {
  static readonly type = '[Category] getCategory';
  constructor(public payload: any) { }
}

export class SearchBusinessesAction {
  static readonly type = '[Businesses] Search';
  constructor(public payload: any) { }
}

export class GetReviewsAction {
  static readonly type = '[Reviews] Get';
  constructor(public payload: any) { }
}

export class CreateReviewAction {
  static readonly type = '[Reviews] create';
  constructor(public payload: any) { }
}