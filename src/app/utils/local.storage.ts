import { Injectable, Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

// key that is used to access the data in local storage
@Injectable()
export class LocalStorageService {
    constructor(@Inject(SESSION_STORAGE) private storage: StorageService) { }

    public storeOnLocalStorage(key: string, task: any): void {
        const value = this.storage.get(key) || null;
        this.storage.set(key, task);
    }

    public removeFromLocalStorage(key: string): void {
        this.storage.remove(key);
    }

    public loadFromLocalStorage(key: string): any {
        const value = this.storage.get(key) || null;
        return value;
    }
}
