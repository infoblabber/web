import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from './local.storage';
import { environment } from '../../environments/environment';
import { User } from '../models/User';

@Injectable()
export class PreventLoggedInAccess implements CanActivate {
  public loggedBefore: boolean;

  constructor(private router: Router, private localStorage: LocalStorageService) {
    this.loggedBefore = false;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if (this.loggedInFlag()) {
      return true;
    }
    this.router.navigate(['']);
    return false;
  }

  loggedInFlag() {
    if (this.localStorage.loadFromLocalStorage(environment.getUser) as User) {
      this.loggedBefore = true;
      return true;
    }
    this.loggedBefore = false;
    return false;
  }
}
