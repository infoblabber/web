export interface Area {
  id: number;
  name: string;
  lat: string;
  lng: string;
  business_count: number;
}
