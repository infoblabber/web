import { BaseResponse } from './BaseResponse';
import { Area } from './Area';
export interface Areas extends BaseResponse {
  areas: Area[];
}
