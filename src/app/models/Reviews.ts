import { BaseResponse } from './BaseResponse';
import { Pagination } from './Pagination';
import { Review } from './Review';

export interface Reviews extends BaseResponse {
    pagination: Pagination;
    reviews: Review[];
}
