import { BaseResponse } from './BaseResponse';
import { Branch } from './Branch';
export interface Business extends BaseResponse {
  id: number;
  name: string;
  main_image: string;
  is_favorite: boolean;
  phone: string;
  rating: number;
  no_of_views: number;
  branch: Branch;
}
