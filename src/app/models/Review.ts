
import { Business } from './Business';
import { UserData } from './UserData';

export interface Review {
    business: Business;
    user: UserData;
    id: number;
    text: string;
}
