export interface Follow {
  id: number;
  user_id: number;
  receiver_id: string;
}
