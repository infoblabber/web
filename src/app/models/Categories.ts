
import { BaseResponse } from './BaseResponse';
import { Category } from './Category';

export interface Categories extends BaseResponse {
  categories: Category[];
}
