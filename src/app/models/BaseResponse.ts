export interface BaseResponse {
  status: string;
  errors: string;
}
