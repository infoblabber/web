import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ProfileComponent } from './components/profile/profile.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BusinessesComponent } from './components/businesses/businesses.component';
import { BusinessDetailsComponent } from './components/business-details/business-details.component';
import { PreventLoggedInAccess } from './utils/login.access';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'signup',
    component: SignupComponent,
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [PreventLoggedInAccess]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [PreventLoggedInAccess]
  },

  {
    path: 'businesses',
    component: BusinessesComponent,
    canActivate: [PreventLoggedInAccess],
  },

  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [PreventLoggedInAccess],
  },

  {
    path: 'details',
    component: BusinessDetailsComponent,
    canActivate: [PreventLoggedInAccess]
  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
